package net.nilosplace.GraphHandle;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class GraphHandle extends JFrame {

	private Figure graph;
	private int offsetx;
	private int offsety;
	private int x,y,z;
	private int width,height;
	private double xc,yc,zc;

	private double THETA = 4 * Math.PI/180;

	public GraphHandle(String init_file, int w, int h) {
		super("Wire Frame Viewer");
		width = w;
		height = h;
		graph = new Figure(init_file);




		JPanel contentPane = new JPanel() {
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				g.setColor(Color.black);
				// System.out.println("This is x " + x + " this is y " + y);
				g.translate(width/2, height/2);
				graph.draw(g);
				g.translate(-width/2, -height/2);
			}
		};
		contentPane.setBackground(Color.white);

		setSize(width, height);
		contentPane.setLayout(new FlowLayout());
		setContentPane(contentPane);

		addWindowListener(new WindowAdapter() { public void windowClosing(WindowEvent e) { System.exit(0); } });

		setVisible(true);


		addMouseListener( new MouseAdapter() {
			public void mousePressed (MouseEvent e) {
				offsetx = x-e.getX();
				offsety = y-e.getY();
			}
		});

		addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseDragged (MouseEvent e) {
				double scale;
				double angle = THETA;
				int newx = e.getX();
				int newy = e.getY();

				System.out.println("This is newx " + newx + " this is newy " + newy);
				System.out.println("This is x " + x + " this is y " + y);

				xc = newx + offsetx - x;
				yc = newy + offsety - y;
				System.out.println("This is xc " + xc + " this is yc " + yc + "\n");

				if(xc < 0) scale = .9;
				else if(xc == 0) scale = 1.0;
				else scale = 1.1;

				x = newx+offsetx;
				y = newy+offsety;

				if(yc < 0) angle = -angle;

				System.out.println("This is x " + x + " this is y " + y);

				System.out.println("This is xc " + scale + " this is yc " + scale);

				graph.scale(scale);
				graph.rotate(angle);
				graph.printPoints();

				repaint();
			}
		});


		final JSlider xs = new JSlider(JSlider.HORIZONTAL);
		final JSlider ys = new JSlider(JSlider.HORIZONTAL);
		final JSlider zs = new JSlider(JSlider.HORIZONTAL);
		final JSlider zoom = new JSlider(1, -100, 100, 10);
		final JTextArea zoom_text = new JTextArea();

		xs.addChangeListener(new ChangeListener() { public void stateChanged(ChangeEvent e) { x = xs.getValue(); } });
		ys.addChangeListener(new ChangeListener() { public void stateChanged(ChangeEvent e) { y = ys.getValue(); } });
		zs.addChangeListener(new ChangeListener() { public void stateChanged(ChangeEvent e) { z = zs.getValue(); } });
		
		zoom.addChangeListener(new ChangeListener() { public void stateChanged(ChangeEvent e) { 
			System.out.println("This is the get val " + zoom.getValue());
			zoom_text.setText(Double.toString(((double)zoom.getValue()/(double)1000)));
			//graph.scale((double)((double)zoom.getValue()/(double)1000)); 
			repaint();
		} });

		JPanel contentPane_temp = new JPanel() {
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
			}
		};
		contentPane_temp.setBackground(Color.white);
		contentPane_temp.setLayout(new FlowLayout());


		contentPane_temp.add(xs);
		contentPane_temp.add(ys);
		contentPane_temp.add(zs);
		contentPane_temp.add(zoom);

		JFrame test = new JFrame();
		test.setContentPane(contentPane_temp);
		test.setSize(100, 300);
		test.setVisible(true);
	}

	public static void main(String[] args) {
		GraphHandle foo = new GraphHandle("cross.vpt", 800, 800);
	}

}
