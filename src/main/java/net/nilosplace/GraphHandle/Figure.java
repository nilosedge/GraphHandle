package net.nilosplace.GraphHandle;
import java.awt.Graphics;
//import Edge;
//import CoolPoint;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

public class Figure {

	public Edge[] edges;
	public Point[] verts;

	public Figure (String init_file) {
		try {
			BufferedReader in = new BufferedReader(new FileReader(new File(init_file)));
			String temp;
			temp = in.readLine();
			verts = new Point[Integer.parseInt(temp)];
			int i = 0;
			while(i < verts.length) {
				temp = in.readLine();
				StringTokenizer tok = new StringTokenizer(temp);
				if(!(temp.length() == 0)) {
					verts[i++] = new Point(Double.parseDouble(tok.nextToken()), Double.parseDouble(tok.nextToken()), Double.parseDouble(tok.nextToken()));
				}
			}
			for(temp = in.readLine(); temp.length() == 0; temp = in.readLine()) {}
			edges = new Edge[Integer.parseInt(temp)];
			i = 0;
			while(i < edges.length) {
				temp = in.readLine();
				if(!(temp.length() == 0)) {
					StringTokenizer tok = new StringTokenizer(temp);
					edges[i++] = new Edge(Integer.parseInt(tok.nextToken()),Integer.parseInt(tok.nextToken()));
				}
			}
		} catch (FileNotFoundException foo) {
			System.out.println("The file " + init_file + " was not found");
		} catch (IOException foo) {
			System.out.println("An IO Except happened... oops");
		}
	}

	public void rotate(double theta) {
		double COS_5 = Math.cos(theta);
		double SIN_5 = Math.sin(theta);
		double x;
		double y;
		for(int i = 0; i < verts.length; i++) {
			x = verts[i].x;
			y = verts[i].y;
			verts[i].x = x * COS_5 - y * SIN_5;
			verts[i].y = x * SIN_5 + y * COS_5;
			if(verts[i].x == 0) verts[i].x = x;
			if(verts[i].y == 0) verts[i].y = y;
		}

	}
	
	public void scale(double scale) {
		System.out.println("This is the new scale " + scale);
		for(int i = 0; i < verts.length; i++) {
			verts[i].x = (verts[i].x*scale);
			verts[i].y = (verts[i].y*scale);
		}
	}
	
	public void printPoints() {
		for(int i = 0; i < verts.length; i++) {
			System.out.println("Point " + i + "--> (" + verts[i].x + "," + verts[i].y + ")");
		}
	}

	public void draw(Graphics g) {
		for(int i = 0; i < edges.length; i++) {
			g.drawLine((int)verts[edges[i].v1].x, (int)verts[edges[i].v1].y, (int)verts[edges[i].v2].x, (int)verts[edges[i].v2].y);
		}
	}

}
