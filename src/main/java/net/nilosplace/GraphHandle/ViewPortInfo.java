package net.nilosplace.GraphHandle;
public class ViewPortInfo {

	private Point ref;
	private Point norm; 
	private Point up;
	private double dist = -10;

	public ViewPortInfo() {
		ref.set_pos(0,0,0);
	}

	public void Transform(Figure points) {
		Matrix start = new Matrix(true);
		
		double L = Math.sqrt(((norm.x*norm.x) + (norm.y*norm.y) + (norm.z*norm.z)));

		Point newnorm = new Point((norm.x*dist/L), (norm.y*dist/L), (norm.z*dist/L));

		double P = Math.sqrt(((newnorm.y* newnorm.y) + (newnorm.z*newnorm.z)));

		Matrix T_r = new Matrix(false);
		T_r.m[3][0] = -ref.x;
		T_r.m[3][1] = -ref.y;
		T_r.m[3][2] = -ref.z;

		Matrix T_n = new Matrix(false);
		T_n.m[3][0] = newnorm.x;
		T_n.m[3][1] = newnorm.y;
		T_n.m[3][2] = newnorm.z;

		Matrix R_y = new Matrix(false);
		R_y.m[0][0] = P/dist;
		R_y.m[0][2] = newnorm.x/dist;
		R_y.m[2][0] = -newnorm.x/dist;
		R_y.m[2][2] = P/dist;

		Matrix R_x = new Matrix(false);
		R_x.m[1][1] = newnorm.z/P;
		R_x.m[2][2] = newnorm.z/P;
		R_x.m[2][1] = -newnorm.y/P;
		R_x.m[1][2] = newnorm.y/P;

	}

}
