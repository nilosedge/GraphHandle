all: build


build: clean Edge.java Figure.java GraphHandle.java
	javac GraphHandle.java

clean:
	rm -fr *.class

run: build
	java GraphHandle
